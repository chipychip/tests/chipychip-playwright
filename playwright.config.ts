import test, {Locator, Page, PlaywrightTestConfig, expect} from '@playwright/test';
import * as dotenv from 'dotenv';
import fs from 'fs';
import {ToastTypes} from './helpers/common';

dotenv.config();

const config: PlaywrightTestConfig = {
  testDir: 'tests',
  /* Maximum time one test can run for. */
  timeout: 2 * 60 * 1000,
  expect: {
    /**
     * Maximum time expect() should wait for the condition to be met.
     * For example in `await expect(locator).toHaveText();`
     */
    timeout: 10000,
    toHaveScreenshot: {
      maxDiffPixelRatio: 0.0001,
    },
  },
  /* Run test-examples in files in parallel */
  fullyParallel: true,
  /* Fail the build on CI if you accidentally left test.only in the source code. */
  forbidOnly: !!process.env.CI,
  /* Retry on CI only */
  retries: 0,
  /* Opt out of parallel test-examples on CI. */
  workers: process.env.CI ? 1 : undefined,
  /* Reporter to use. See https://playwright.dev/docs/test-reporters */
  reporter: [
    ['list'],
    ['html', {open: 'never'}],
    [
      'allure-playwright',
      {
        detail: true,
      },
    ],
  ],
  /* Shared settings for all the projects below. See https://playwright.dev/docs/api/class-testoptions. */
  use: {
    /* Maximum time each action such as `click()` can take. Defaults to 0 (no limit). */
    actionTimeout: 5000,
    baseURL: process.env.BASE_URL,
  },
  projects: [
    {
      name: 'LOCAL Chromium',
      use: {
        viewport: {width: 1920, height: 1080},
        browserName: 'chromium',
        headless: true,
        video: 'on',
        trace: 'on',
        screenshot: 'on',
        launchOptions: {
          slowMo: 0,
        },
      },
    },
  ],
};

expect.extend({
  async toHaveScreenshotAttach(
    actual: any,
    name: string,
    options?: {
      animations?: 'disabled' | 'allow';
      caret?: 'hide' | 'initial';
      clip?: {
        x: number;
        y: number;
        width: number;
        height: number;
      };
      fullPage?: boolean;
      mask?: Array<Locator>;
      maskColor?: string;
      maxDiffPixelRatio?: number;
      maxDiffPixels?: number;
      omitBackground?: boolean;
      scale?: 'css' | 'device';
      threshold?: number;
      timeout?: number;
    }
  ) {
    try {
      const screenshot = await actual.screenshot(options);
      await test.info().attach(name, {body: screenshot, contentType: 'image/png'});
      await expect(actual).toHaveScreenshot(name, options);

      return {
        message: () => `Screenshots "${name}" are equal`,
        pass: true,
      };
    } catch (error: any) {
      return {
        message: () => error.message,
        pass: false,
      };
    }
  },

  async toHaveToast(
    page: Page,
    options?: {type?: ToastTypes; timeout?: number; toastMessage?: string}
  ): Promise<{message: () => string; pass: boolean}> {
    const {type, timeout, toastMessage} = options ?? {};
    try {
      const toast = page.locator('//*[contains(@class, "Toastify__toast")]');
      await expect(toast.first()).toBeVisible({timeout: timeout ?? 5000});
      const toasts = await toast.all();
      if (toasts.length > 1) {
        const messages = await Promise.all(toasts.map((t) => t.innerText()));
        return {
          message: () => `Several toasts found: ${messages.join(', ')}`,
          pass: false,
        };
      }
      if (type) {
        await expect(toast).toHaveClass(new RegExp(`(.+|)toast--${type}(.+|)`));
      }
      if (toastMessage) {
        await expect(toast).toHaveText(toastMessage);
      }
      if (!type && !toastMessage) {
        await expect(toast).toHaveClass(new RegExp(`(.+|)toast-${ToastTypes.SUCCESS}(.+|)`));
      }

      return {
        message: () => `Toast was successful`,
        pass: true,
      };
    } catch (error: any) {
      return {
        message: () => error.message,
        pass: false,
      };
    }
  },
});

export default config;
