## ChipyChip test automation framework

### Installation

- install [node v18.19.0](https://nodejs.org/en/blog/release/v18.19.0/);
- install dependencies: `npm install`
- install browser binaries: `npx playwright install`

### Env file
- copy .env file: `cp .env.example .env`

### Usage

#### Prettier setup

For VSCode

- Download plugin [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

For JetBrains IDEA

- Download plugin [Prettier](https://prettier.io/)
- Go to Preference->Tool->Actions on Save turn on "Run Prettier"

#### Run tests using terminal

- Running all tests: `npx playwright test`
- Running a single test file: `npx playwright test tests/testName.spec.ts`
- IMPORTANT run the written test using this command: `npx playwright test tests/testName.spec.ts --workers=5`

#### Run tests using docker

- use command in terminal: `docker run --rm --network host -v $(pwd):/work/ -w /work/ -it mcr.microsoft.com/playwright:v1.44.1-focal /bin/bash`

#### Run tests using plugin

For VSCode

- Download plugin [Playwright Runner](https://marketplace.visualstudio.com/items?itemName=ortoni.ortoni) (free, works
  more stable)

For JetBrains IDEA

- Download plugin [Maestro](https://plugins.jetbrains.com/plugin/18100-maestro) (paid,works not stable)

#### Test Generator

- use command in terminal: `npx playwright codegen URL`

#### Trace Viewer

- use command in terminal: `npx playwright show-report`
