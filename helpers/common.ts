import {faker} from '@faker-js/faker';
import _ from 'lodash';
import {Locator} from '@playwright/test';

export enum ToastTypes {
  SUCCESS = 'success',
  WARNING = 'warning',
}

export const randomUUID = (): string => {
  return _.replace(faker.datatype.uuid(), /-/gm, '');
};

export async function getInnerTextWithoutLineBreaks(cell: Locator): Promise<string> {
  const text = await cell.innerText();
  return text.replace(/(\r\n|\n|\r)/gm, '');
}
