import {Locator, expect, test} from '@playwright/test';

interface FillOptions {
  delay?: number;
  timeout?: number;
}

export class Input {
  private readonly inputLocator: Locator;

  constructor(input: Locator) {
    this.inputLocator = input;
  }

  async fill(value: string, options: FillOptions = {}): Promise<void> {
    const {delay = 0, timeout = 5000} = options;

    await test.step(`Type value: ${value}`, async () => {
      await expect(this.inputLocator, 'Wait until input is visible').toBeVisible();
      await expect(this.inputLocator, 'Wait until input is enabled').toBeEnabled();
      await this.inputLocator.click();
      await this.inputLocator.clear();
      await this.inputLocator.type(value, {delay, timeout});
    });
  }

  async removeFocus(): Promise<void> {
    await test.step('Remove focus from input', async () => {
      await expect(this.inputLocator).toBeFocused();
      await this.inputLocator.blur();
      await expect(this.inputLocator).not.toBeFocused();
    });
  }

  async getValue(): Promise<string> {
    return test.step('Get input value', async () => {
      await expect(this.inputLocator).toBeVisible();
      await expect(this.inputLocator).toBeEnabled();
      return (await this.inputLocator.getAttribute('value')) as string;
    });
  }
}
