import {expect, Locator, test} from '@playwright/test';

export class Button {
  private readonly buttonLocator: Locator;
  private readonly loadingCat: Locator;

  constructor(button: Locator) {
    this.buttonLocator = button;
    this.loadingCat = this.buttonLocator.page().locator('//*[@data-testid="loading-cat"]');
  }

  async click() {
    await expect(this.buttonLocator, 'Wait until button is visible').toBeVisible();
    await expect(this.buttonLocator, 'Wait until button is enabled').toBeEnabled();
    await this.buttonLocator.click();
  }
}
