import {ToastTypes} from '@helpers/common';

export {};

declare global {
  namespace PlaywrightTest {
    interface Matchers<R, T> {
      toHaveScreenshotAttach(
        name: string,
        options?: {
          animations?: 'disabled' | 'allow';
          caret?: 'hide' | 'initial';
          clip?: {
            x: number;
            y: number;
            width: number;
            height: number;
          };
          fullPage?: boolean;
          mask?: Array<Locator>;
          maskColor?: string;
          maxDiffPixelRatio?: number;
          maxDiffPixels?: number;
          omitBackground?: boolean;
          scale?: 'css' | 'device';
          threshold?: number;
          timeout?: number;
        }
      ): Promise<R>;
      toHaveToast(options?: {type?: ToastTypes; timeout?: number; toastMessage?: string}): Promise<R>;
    }
  }
}
