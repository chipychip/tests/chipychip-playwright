import {BasePage} from 'pages/BasePage';
import test, {expect, Locator, Page} from '@playwright/test';
import {Button} from 'elements/Button';
import {getInnerTextWithoutLineBreaks} from 'helpers/common';

enum CardTabs {
  PREVIEW = 'Preview',
  DRAFT = 'Draft',
}

export class CardPage extends BasePage {
  public readonly header: Header;

  constructor(page: Page) {
    super(page, '/сhipyсhip-card');
    this.header = new Header(page);
  }
}

class Header {
  private readonly page: Page;
  private readonly header: Locator;
  private readonly title: Locator;
  private readonly logoutButton: Button;

  constructor(page: Page) {
    this.page = page;
    this.header = page.locator('header');
    this.title = this.header.locator('h1');
    this.logoutButton = new Button(this.header.locator('Logout'));
  }

  async openTab(tab: CardTabs): Promise<void> {
    await test.step(`Open tab "${tab}"`, async () => {
      await new Button(this.header.locator(tab)).click();
    });
  }

  async getTabState(tab: CardTabs): Promise<boolean> {
    return test.step(`Get tab "${tab}" state`, async () => {
      return (await this.header.locator(tab).getAttribute('aria-selected')) === 'true';
    });
  }

  async getTitle(): Promise<string> {
    return test.step('Get title', async () => {
      await expect(this.title, 'Wait until title is visible').toBeVisible();
      return getInnerTextWithoutLineBreaks(this.title);
    });
  }

  async logout(): Promise<void> {
    await test.step('Logout', async () => {
      await this.logoutButton.click();
      await this.page.context().clearCookies();
      await this.page.reload();
    });
  }
}
