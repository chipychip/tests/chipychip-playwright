import {Locator, Page} from '@playwright/test';

export class BasePage {
  readonly page: Page;
  public readonly pageUrl: string;
  public readonly loadingCat: Locator;

  constructor(page: Page, pageUrl: string) {
    this.page = page;
    this.pageUrl = pageUrl;
    this.loadingCat = this.page.locator('//*[@data-testid="loading-cat"]');
  }

  async goto(options?: object) {
    let pageUrl: string = this.pageUrl;
    for (const key in options) {
      const regexp = new RegExp('\\{' + key + '\\}', 'gi');
      pageUrl = pageUrl.replace(regexp, options[key]);
    }
    await this.page.goto(pageUrl);
  }
}
