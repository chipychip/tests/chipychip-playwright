import {BasePage} from 'pages/BasePage';
import {expect, Locator, Page, test} from '@playwright/test';
import {Input} from 'elements/Input';
import {Button} from 'elements/Button';
import {getInnerTextWithoutLineBreaks} from 'helpers/common';

export class SignInPage extends BasePage {
  public readonly signInForm: Locator;
  private readonly errorMessage: Locator;
  private readonly emailInput: Input;
  private readonly passwordInput: Input;
  private readonly signInButton: Button;
  private readonly signUpLink: Locator;

  constructor(page: Page) {
    super(page, '/signIn');
    this.signInForm = page.locator('Sign In-form');
    this.errorMessage = this.signInForm.getByTestId('error-message');
    this.emailInput = new Input(this.signInForm.getByTestId('email').locator('//input'));
    this.passwordInput = new Input(this.signInForm.getByTestId('password').locator('//input'));
    this.signInButton = new Button(this.signInForm.getByTestId('Sign In'));
    this.signUpLink = this.signInForm.getByTestId('Sign Up');
  }

  async login(email: string, password: string): Promise<void> {
    await test.step('Login as user', async () => {
      await this.emailInput.fill(email);
      await this.passwordInput.fill(password);
      await this.signInButton.click();
    });
  }
  async confirmSignInSuccessfully(): Promise<void> {
    await expect(this.signInForm, 'Sign In Form should be hidden').toBeHidden();
  }

  async getSignInMessage(): Promise<string> {
    return test.step('Get error message', async () => {
      await expect(this.errorMessage, 'Wait until error message is visible').toBeVisible();
      return await getInnerTextWithoutLineBreaks(this.errorMessage);
    });
  }

  async goToSignUp(): Promise<void> {
    await test.step('Go to Sign Up page', async () => {
      await this.signUpLink.click();
    });
  }
}
